import { useRouter } from "next/router";
import React, { useEffect, useRef, useState } from "react";
import { Button, PageHeader, Descriptions, Input, message } from "antd";

import { withContextInitialized } from "../../components/hoc";
import CompanyCard from "../../components/molecules/CompanyCard";
import GenericList from "../../components/organisms/GenericList";
import OverlaySpinner from "../../components/molecules/OverlaySpinner";
import { usePersonInformation } from "../../components/hooks/usePersonInformation";

import { Company } from "../../constants/types";
import { ResponsiveListCard } from "../../constants";

const PersonDetail = () => {
  const router = useRouter();
  const [showInputs, setShowInputs] = useState(false);
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );
  const [newUserDate, setNewUserData] = useState(data);

  useEffect(() => {
    load();
  }, []);

  if (loading) {
    return (
      <OverlaySpinner title={`Loading ${router.query?.email} information`} />
    );
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push("/home")
    );
    return <></>;
  }

  return (
    <>
      <PageHeader
        onBack={router.back}
        title='Person'
        subTitle='Profile'
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type='link'
            href={data.website}
            target='_blank'
            rel='noopener noreferrer'
          >
            Visit website
          </Button>,
          <Button type='default' onClick={() => setShowInputs(!showInputs)}>
            Edit
          </Button>,
        ]}
      >
        {data && (
          <Descriptions size='small' column={1}>
            <Descriptions.Item label='Name'>{data.name}</Descriptions.Item>
            <Descriptions.Item label='Gender'>{data.gender}</Descriptions.Item>
            <Descriptions.Item label='Phone'>{data.phone}</Descriptions.Item>

            <Descriptions.Item label='Birthday'>
              {data.birthday}
            </Descriptions.Item>
          </Descriptions>
        )}
        {showInputs && (
          <form
            onSubmit={(e) => {
              e.preventDefault();

              setNewUserData({
                name: e.target[0].name.value,
                gender: e.target[1].name.value,
                phone: e.target[2].name.value,
                birthday: e.target[3].name.value,
              });

              save(newUserDate);
            }}
          >
            <input type='text' id='Name' defaultValue={data.name} />
            <input type='text' id='Gender' defaultValue={data.gender} />
            <input type='text' id='Phone' defaultValue={data.phone} />
            <input type='text' id='Birthday' defaultValue={data.birthday} />
            <button>Save</button>
          </form>
        )}
        <GenericList<Company>
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
          handleLoadMore={() => {}}
          hasMore={false}
        />
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonDetail);
